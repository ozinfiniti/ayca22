import sqlite3

conn = sqlite3.connect('db.sqlite3')
c = conn.cursor()
c.execute('''CREATE TABLE users
             (id  INTEGER PRIMARY KEY,
              user_name varchar(30) NOT NULL,
              password varchar(1000) NOT NULL,
              active bool NULL, 
              last_login datetime NULL,
              ip char(39) NULL)''')
conn.commit()
conn.close()