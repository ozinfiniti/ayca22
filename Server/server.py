# server.py
import sqlite3
import socket
import time
import pickle


def register(username, password, active, date, ip_addr):
    conn = sqlite3.connect('db.sqlite3')
    c = conn.cursor()
    try:
        c.execute("INSERT INTO app_user(user_name, password, active, last_login, ip) VALUES (?, ?, ?, ?, ?);", (username, password, active, date, ip_addr))
        conn.commit()
        conn.close()
        return {'responseCode': 1, 'responseMessage': 'Succesfully registered :Smiley'}
    except sqlite3.IntegrityError:
        conn.close()
        return {'responseCode': 0, 'responseMessage': 'User exists!!!'}


def login(username, password):
    conn = sqlite3.connect('db.sqlite3')
    c = conn.cursor()
    c.execute("SELECT password FROM app_user WHERE user_name=?;", (username, ))
    user = c.fetchone()
    try:
        if user[0] == password:
            currentTime = time.ctime(time.time())
            c.execute("UPDATE app_user SET active=?,last_login=? WHERE user_name=?;", (True, currentTime, username))
            conn.commit()
            conn.close()
            return {'responseCode': 1, 'responseMessage': 'Successfully Loged in'}
        else:
            return {'responseCode': 0, 'responseMessage': 'Failed to auth user!!!'}
    except TypeError:
        return {'responseCode': 5, 'responseMessage': 'Please Register :Smiley'}


def logout(ip):
    conn = sqlite3.connect('db.sqlite3')
    c = conn.cursor()
    c.execute("UPDATE app_user SET active=?, last_login=? WHERE ip=?;", (False, None, ip))
    conn.commit()
    conn.close()
    return {'responseCode': 1, 'responseMessage': 'Successfully Loged OUT!!!'}


tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# get local machine name
host = socket.gethostname()
print host
BUFFER_SIZE = 1024
tcp_port = 9999
udp_port = 8888

# bind to the port
tcp_socket.bind(('', tcp_port))
# udp_socket.bind(('', udp_port))

# queue up to 5 requests
tcp_socket.listen(5)
# udp_socket.listen(5)

while True:
    result = {}
    # establish a connection
    clientsocket, addr = tcp_socket.accept()
    # print clientsocket.recv(BUFFER_SIZE)
    data = pickle.loads(clientsocket.recv(BUFFER_SIZE))
    func = data['callingFunction']

    if func == 'login':
        user_name = data['username']
        password = data['password']
        result = login(username=user_name, password=password)
        """
        login
        """
    elif func == 'logout':
        ip = data['ipAddress']
        result = logout(ip=ip)
        """
        search
        """
    elif func == 'register':
        user_name = data['username']
        password = data['password']
        ip = data['ipAddress']
        result = register(username=user_name, password=password, active=False, date=None, ip_addr=ip)
        """
        register
        """
    elif func == 'connect':
        pass
        """
        connect
        """
    else:
        pass

    print("Got a connection from %s" % str(addr))
    print result
    currentTime = time.ctime(time.time()) + "\r\n"
    # clientsocket.send(currentTime.encode('ascii'))
    clientsocket.send(pickle.dumps(result))
    clientsocket.close()
