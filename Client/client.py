# client.py

import thread
import time
import tkinter as tk
import socket
import pickle

def message(content):
    return {"callingFunction": "message", "message": content}

class Chat:
    def __init__(self, root):
        self.root = root
        self.loginMenu()

        self.serverAddress = '192.168.1.27'
        self.serverPortTCP = 9999
        self.serverPortUDP = 8888

    def connectionTCP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # host = socket.gethostname()
        s.connect((self.serverAddress, self.serverPortTCP))
        self.s = s

    def sendRequestTCP(self, data):
        self.connectionTCP()
        s = self.s
        data['ipAddress'] = s.getsockname()[0]
        s.send(pickle.dumps(data))
        tm = pickle.loads(s.recv(1024))
        s.close()
        return tm

    def connectionUDP(self):
        socketUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socketUDP = socketUDP
        self.helloMessage = thread.start_new_thread(self.sendRequestUDP, ())

    def sendRequestUDP(self):
        while self.isLogin == True:
            data = {"callingFunction": "hello", "username": self.username}
            self.socketUDP.sendto(pickle.dumps(data), (self.serverAddress, self.serverPortUDP))
            time.sleep(60)




    def searchMenu(self):
        tk.Label(self.root, text="Search User: ").grid(row=0, sticky=tk.E)
        self.searchingUsernameET = tk.Entry(root)
        self.searchingUsernameET.grid(row=0, column=1)
        tk.Button(self.root, text="Search", command=self.search).grid(row=2, column=0)
        tk.Button(self.root, text="Logout", command=self.logout).grid(row=2, column=1)
        tk.Button(self.root, text="Change User", command=self.changeUser).grid(row=2, column=2)

    def loginMenu(self):
        tk.Label(self.root, text="Username").grid(row=0, sticky=tk.E)
        tk.Label(self.root, text="Password").grid(row=1, sticky=tk.E)

        self.usernameET = tk.Entry(self.root)
        self.usernameET.grid(row=0, column=1)
        self.passwordET = tk.Entry(root, show="*")
        self.passwordET.grid(row=1, column=1)

        tk.Button(self.root, text="Login", command=self.login).grid(row=2, column=0)
        tk.Button(self.root, text="Register", command=self.register).grid(row=2, column=1)

    def cleanGUI(self):
        for child in self.root.winfo_children():
            child.destroy()

    def chatScreen(self):
        self.messages = tk.Text(self.root, state="disable")
        self.messages.pack()

        self.inputVar = tk.StringVar()
        self.inputET = tk.Entry(self.root, text=self.inputVar)
        self.inputET.pack(side=tk.BOTTOM, fill=tk.X)

        frame = tk.Frame(self.root)
        self.inputET.bind("<Return>", self.enterClickListener)
        frame.pack()

    def enterClickListener(self, event):
        self.messages.configure(state="normal")
        self.messages.insert(tk.INSERT, '%s\n' % self.inputET.get())
        self.messages.configure(state="disable")
        self.messages.see('end')
        self.inputVar.set("")

    def login(self):
        data = {"callingFunction": "login", "username": self.usernameET.get(), "password": self.passwordET.get()}
        response = self.sendRequestTCP(data)
        responseCode = int(response['responseCode'])
        responseMessage = response['responseMessage']
        if(responseCode == 1):
            self.username = self.usernameET.get()
            self.isLogin = True
            self.cleanGUI()
            self.searchMenu()
            self.connectionUDP()
        else:
            PopupWindow(self.root, responseMessage)

    def register(self):
        data = {"callingFunction": "register", "username": self.usernameET.get(), "password": self.passwordET.get()}
        response = self.sendRequestTCP(data)
        responseCode = response['responseCode']
        responseMessage = response['responseMessage']
        if(responseCode == 1):
            PopupWindow(self.root, responseMessage)
        else:
            PopupWindow(self.root, responseMessage)

    def search(self):
        data = {"callingFunction": "search", "username": self.searchingUsernameET.get()}
        response = self.sendRequestTCP(data)
        responseCode = response['responseCode']
        responseMessage = response['responseMessage']
        if (responseCode == 1):
            pass
            # self.s.close()
            #peer 2 peer chat acilacak
        else:
            PopupWindow(self.root, responseMessage)

    def logout(self):
        data = {"callingFunction": "logout", "username": self.username}
        response = self.sendRequestTCP(data)
        responseCode = response['responseCode']
        responseMessage = response['responseMessage']
        if (responseCode == 1):
            # self.s.close()
            self.isLogin = False
            PopupWindow(self.root, responseMessage, True)
        else:
            PopupWindow(self.root, responseMessage)

    def changeUser(self):
        data = {"callingFunction": "logout", "username": self.username}
        response = self.sendRequestTCP(data)
        responseCode = response['responseCode']
        responseMessage = response['responseMessage']
        if (responseCode == 1):
            # self.s.close()
            self.isLogin = False
            PopupWindow(self.root, responseMessage)
            self.cleanGUI()
            self.loginMenu()
        else:
            PopupWindow(self.root, responseMessage)

class PopupWindow:
    def __init__(self, root, data , *args):
        self.root = root
        self.popup = tk.Toplevel(root)
        self.popup.wm_title(" ")
        popupMessage = tk.Label(self.popup, text=data)
        popupMessage.pack(side="top", fill="both")
        try:
            if(args[0] is not None and args[0] == True):
                self.okeyButton = tk.Button(self.popup, text="Close", command=self.destroyMainRoot)
        except Exception:
            self.okeyButton = tk.Button(self.popup, text="Okey", command=self.closePopup)
        self.okeyButton.pack(side="top")

    def closePopup(self):
        self.popup.destroy()

    def destroyMainRoot(self):
        self.root.destroy()

if __name__ == "__main__":
    root = tk.Tk()
    chatGUI = Chat(root)
    root.mainloop()